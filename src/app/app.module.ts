import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage1, QuizDetailsPage1, QuizInteractionPage1, BarcodeScanerPage1 } from '../pages/pages';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '../providers/user/user';
import { Camera } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { HttpModule, JsonpModule  } from '@angular/http';
import { RestProvider } from '../providers/rest/rest';
import { QuizSolverProvider } from '../providers/quiz-solver/quiz-solver';
import { TimerProvider } from '../providers/timer/timer';
import { StatisticsPage } from '../pages/statistics/statistics';
import { QuizResultPage } from '../pages/quiz-result/quiz-result';
import { QuizesSolvedPage } from '../pages/quizes-solved/quizes-solved';
import { QuizDetailsPage } from '../pages/quiz-details/quiz-details';
import { UserPage } from '../pages/user/user';
import { QuizSolvedResponsesPage } from '../pages/quiz-solved-responses/quiz-solved-responses';
import { RegisterPage } from '../pages/register/register';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage1,
    QuizDetailsPage1,
    QuizInteractionPage1,
    BarcodeScanerPage1,
    StatisticsPage,
    QuizResultPage,
    QuizesSolvedPage,
    UserPage,
    QuizSolvedResponsesPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule, 
    JsonpModule 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage1,
    QuizDetailsPage1,
    QuizInteractionPage1,
    BarcodeScanerPage1,
    StatisticsPage,
    QuizResultPage,
    QuizDetailsPage,
    QuizesSolvedPage,
    UserPage,
    QuizSolvedResponsesPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    Camera,
    BarcodeScanner,
    RestProvider,
    QuizSolverProvider,
    TimerProvider
  ]
})
export class AppModule {}
