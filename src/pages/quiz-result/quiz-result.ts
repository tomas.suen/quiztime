import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';

/**
 * Generated class for the QuizResultPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var Chart;
@IonicPage()
@Component({
  selector: 'page-quiz-result',
  templateUrl: 'quiz-result.html',
})
export class QuizResultPage {

  koncano: any;
  availableTime: any;
  quiz: any;
  odgovor: any;
  poskus: any;

  time: any; timeAvailable: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.poskus = navParams.get("data").poskus;
    this.quiz = navParams.get("data").quiz;
    this.odgovor = navParams.get("data").odgovori;

    console.log("parameter", navParams.get("data"));
    this.koncano = moment(moment(this.poskus.responseEnd).diff(moment(this.poskus.responseStart))).format("mm:ss");

    console.log("koncano", this.koncano);

    this.availableTime = this.quiz.duration + ":00";

    let user = localStorage.getItem("user")
    localStorage.clear();
    localStorage.setItem("user", user);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizResultPage');

    var ctx = document.getElementById("myChart1");
    var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels: ["Fail", "Ok"],
        datasets: [{
          data: [100-this.poskus.score, this.poskus.score],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 235, 162, 0.2)',
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 235, 162, 1)',
          ],
          borderWidth: 1
        }]
      }
    });
  }

}
