import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { ListPage } from '../list/list';

/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  account: { username: string, password1: string, password2: string } =
  { username: '', password1: '', password2: '' };
  ;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public rest: RestProvider,
    public menu: MenuController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  doRegistration() {
    if (this.account.password1.localeCompare(this.account.password2)) {
      alert("Passwords do not match");
      this.account.password1 = this.account.password2 = "";
      return;
    }

    this.rest.usernameCheck(this.account).subscribe((a) => {
      if (a === null) {
        this.rest.register(this.account).subscribe((x) => {
          let zac = { "id": x.id, "username": x.username };
          localStorage.setItem("user", JSON.stringify(zac));

          this.navCtrl.setRoot(ListPage);
          this.menu.enable(true, "sideMenu")
        })
      }
      else {
        alert("Username is takken");
        this.account.password1 = this.account.password2 = "";
        return;
      }
    });


  }

}
