import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BarcodeScanerPage } from './barcode-scaner';

@NgModule({
  declarations: [
    BarcodeScanerPage,
  ],
  imports: [
    IonicPageModule.forChild(BarcodeScanerPage),
  ],
  exports: [
    BarcodeScanerPage
  ]
})
export class BarcodeScanerPageModule {}
