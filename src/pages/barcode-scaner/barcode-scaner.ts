import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the BarcodeScanerPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-barcode-scaner',
  templateUrl: 'barcode-scaner.html',
})

export class BarcodeScanerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BarcodeScanerPage');
  }

  click() {
    this.barcodeScanner.scan()
      .then((result) => {
        if (!result.cancelled) {
          console.log( result.text, result.format);
        }
      })
      .catch((err) => {
        alert(err);
      })
  }

}
