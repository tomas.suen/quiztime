import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the UserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  account: { id: number, username: string, password: string, password1: string, password2: string } =
  { id: -1, username: '', password: '', password1: '', password2: '' };
  ;

  constructor(public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider) {
    if (this.rest.loginCheck(navCtrl)) {
      this.account.id = JSON.parse(localStorage.getItem("user")).id;
      this.account.username = JSON.parse(localStorage.getItem("user")).username;
    }

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  doUpdate() {
    if (this.account.password1.localeCompare(this.account.password2)) {
      alert("Passwords do not match");
      this.account.password1 = this.account.password2 = "";
      return;
    }

    this.rest.login(this.account).subscribe(x => {
      if (x === null) {
        alert("Wrong password");
      }
      else {
        this.rest.userUpdatePassword(this.account).subscribe(d => { alert("password changed");} );
      }
    }, err => {
      console.log("napaka v prijavi", err);
    }); 

  }
}
