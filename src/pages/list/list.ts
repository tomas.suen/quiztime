// @ts-check 
import { Component, } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { QuizDetailsPage1 } from '../pages';
import { RestProvider } from '../../providers/rest/rest';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { QuizSolverProvider } from '../../providers/quiz-solver/quiz-solver';
import { QuizInteractionPage } from '../quiz-interaction/quiz-interaction';


//Q1: kako se rešim nav back
//solved: kako naredim provider in se lahko obesim na njegov promise/callbac/await

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  items: any;
  private isOn: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private barcodeScanner: BarcodeScanner,
    public rest: RestProvider,
    public quizSolver: QuizSolverProvider) {
    this.rest.loginCheck(navCtrl);
    this.initializeItems();
  }

  toggleDetails() {
    this.isOn = !this.isOn;
    this.initializeItems();
  }

  initializeItems() {
    this.rest.getQuizes().subscribe(
      (data) => {
        this.items = data;
      },
      err => { console.log(err); alert("Can't connect to server"); }
    );
  }

  itemTapped(event, selectedQuiz) {
    this.navCtrl.push(QuizDetailsPage1, { item: selectedQuiz });
  }

  findItems(ev: any) {
    this.rest.getQuizes().subscribe(
      (data) => {
        let val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.items = data.filter((item) => {
            return (item.quizName.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
      },
      err => { console.log(err); alert("Can't connect to server"); }
    );
  }

  barcode() {
    this.barcodeScanner.scan()
      .then((result) => {
        if (!result.cancelled) {
          //TODO start quiz

          console.log(result.text, result.format);
          let scan = JSON.parse(result.text);
          let userID = JSON.parse(localStorage.getItem("user")).id;

          this.rest.getQuizDetailsByid(scan.id).subscribe((quiz) => {
            if (scan.pwd !== quiz.quizPassword) {

              this.quizSolver.reset();

              console.log("kviz z podatki", quiz)
              localStorage.setItem("kviz", JSON.stringify(quiz[0]));
              localStorage.setItem("vprasanja", JSON.stringify(quiz[0].question));

              this.rest.getTime().subscribe((data1) => {
                console.log("quiz started", data1.timestamp);

                this.rest.announceBeginigOfQuiz(userID, quiz[0].id, data1.timestamp).subscribe((dat) => {
                  console.log(dat);
                  localStorage.setItem("poskus", JSON.stringify(dat));
                  this.navCtrl.push(QuizInteractionPage, { "trenutni": 0 });

                }, err => console.log(err));
              }, err => console.log(err));
            }
          });


        }
      })
      .catch((err) => {
        alert(err);
      })
  };



}
