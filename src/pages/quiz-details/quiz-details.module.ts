import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizDetailsPage } from './quiz-details';

@NgModule({
  declarations: [
    QuizDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizDetailsPage),
  ],
  exports: [
    QuizDetailsPage
  ]
})
export class QuizDetailsPageModule {}
