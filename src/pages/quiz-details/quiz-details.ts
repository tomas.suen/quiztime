import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { QuizInteractionPage1 } from '../pages';

import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';
import { TimerProvider } from '../../providers/timer/timer';
import { QuizSolverProvider } from '../../providers/quiz-solver/quiz-solver';


//solved: kako naredim da se komponenta posodobi z avtorjevim imenom?

@IonicPage()
@Component({
  selector: 'page-quiz-details',
  templateUrl: 'quiz-details.html',
})
export class QuizDetailsPage {
  quiz: any;
  username: string;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private rest: RestProvider, public timer: TimerProvider, public quizSolver: QuizSolverProvider) {

    this.rest.loginCheck(navCtrl);
    console.log(JSON.stringify(navParams.get('item')));
    
    this.quiz = navParams.get('item');
    //this.username = "";
    //kaj naj naredim z datomu?
    this.quiz.AvailableFrom = moment(this.quiz.AvailableFrom).format('DD-MMM-YYYY HH:mm');
    this.quiz.AvailableTo = moment(this.quiz.AvailableTo).format('DD-MMM-YYYY HH:mm');
    this.getAuthor();
  }

  getAuthor() {
    this.rest.getUserById(this.quiz.user_id).subscribe(
      data => {
        this.username = data[0].username;

      },
      err => console.log(err)
    );
  }

  ionViewDidLoad() {

  }

  ionViewWillLeave() {

  }

  startQuiz(error?: string) {
    let prompt = this.alertCtrl.create({
      title: 'Begin Quiz',
      message: error + "Enter the password if you wish to begin",
      inputs: [
        {
          name: 'password',
          placeholder: 'password',
          type: 'password'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data0 => {
            console.log("vpisano geslo:", data0.password);
            console.log("zahtevano geslo:", this.quiz.quizPassword);
            if (data0.password === this.quiz.quizPassword) {

              this.quizSolver.reset();
              let userID = JSON.parse(localStorage.getItem("user")).id;

              this.rest.getQuizDetailsByid(this.quiz.id).subscribe(data => {
                console.log("kviz z podatki", JSON.stringify(data[0]))
                localStorage.setItem("kviz", JSON.stringify(data[0]));
                localStorage.setItem("vprasanja", JSON.stringify(data[0].question));

                this.rest.getTime().subscribe((data1) => {
                  console.log("quiz started", data1.timestamp);

                  this.rest.announceBeginigOfQuiz(userID, this.quiz.id, data1.timestamp).subscribe((dat) => {
                    console.log(dat);
                    localStorage.setItem("poskus", JSON.stringify(dat));
                    this.navCtrl.push(QuizInteractionPage1, { "trenutni": 0 });

                  }, err => console.log(err));
                }, err => console.log(err));
              }, err => console.log(err));
            }
            else {
              this.startQuiz("You entered wrong password.\n");
            }
          }
        }
      ]
    });
    prompt.present();
  }
}
