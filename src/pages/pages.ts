import { HomePage } from './home/home';
import { ListPage } from './list/list';
import { LoginPage } from './login/login';
import { QuizDetailsPage } from './quiz-details/quiz-details';
import { QuizInteractionPage } from './quiz-interaction/quiz-interaction';
import { BarcodeScanerPage } from './barcode-scaner/barcode-scaner'

export const HomePage1 = HomePage;
export const ListPage1 = ListPage;
export const LoginPage1 = LoginPage;
export const QuizDetailsPage1 = QuizDetailsPage;
export const QuizInteractionPage1 = QuizInteractionPage;
export const BarcodeScanerPage1 = BarcodeScanerPage;