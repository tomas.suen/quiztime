import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TimerProvider } from '../../providers/timer/timer';

/**
 * Generated class for the StatisticsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

declare var Chart;
@IonicPage()
@Component({
  selector: 'page-statistics',
  templateUrl: 'statistics.html',
})
export class StatisticsPage {

  labels = [];
  values = [];

  timerLabel: string;
  timerTime: number;
  intervalTimer: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public ngZone: NgZone,
    public timer: TimerProvider) {
    console.log(navParams.get("vprasanje").answers);
    this.labels = navParams.get("vprasanje").answers;

    let stat = navParams.get("stat");
    console.log("statistika", stat)
    this.values = this.labels.map( x =>
        {return x.value = stat[x.anwserID.toString()];}
    )

    console.log(this.labels);

    this.labels = this.labels.map(function (a) { return a.anwserText; });
  }

  ionViewDidLeave() {
    clearInterval(this.intervalTimer);
  }

  ionViewDidLoad() {
    this.timerTime = this.timer.getTimer();

    this.intervalTimer = setInterval(() => {
      //console.log("ZMANJŠUJEM TIMER")
      this.timerTime = this.timer.tick();
      this.timerLabel = this.timer.toString();
      if (this.timerTime <= 0) {
        alert("KONEC");
        clearInterval(this.intervalTimer);
      }
    }, 1000);

    console.log('ionViewDidLoad StatisticsPage');
    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.labels,
       datasets: [{
            label: '# of Votes',
            data: this.values,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
      },
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
    });
  }

}
