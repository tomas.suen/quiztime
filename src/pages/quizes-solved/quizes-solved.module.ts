import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizesSolvedPage } from './quizes-solved';

@NgModule({
  declarations: [
    QuizesSolvedPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizesSolvedPage),
  ],
  exports: [
    QuizesSolvedPage
  ]
})
export class QuizesSolvedPageModule {}
