import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { QuizSolvedResponsesPage } from '../quiz-solved-responses/quiz-solved-responses';
import { LoginPage } from '../login/login';

/**
 * Generated class for the QuizesSolvedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-quizes-solved',
  templateUrl: 'quizes-solved.html',
})
export class QuizesSolvedPage {

  responses = [];
  quizes = [];
  isOn: boolean;
  userID: number;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public rest: RestProvider) {

    if (this.rest.loginCheck(navCtrl)) {
      this.menu.enable(true, "sideMenu")
      this.userID = JSON.parse(localStorage.getItem("user")).id;

     this.initializeItems()

    }


  }

  toggleDetails() {
    this.isOn = !this.isOn;
    this.initializeItems();
  }

  initializeItems() {
    
    this.rest.quizesTryedOfPerson(this.userID).subscribe(
      (data) => {
        this.quizes = data;
      },
      err => { console.log(err); alert("Can't connect to server"); }
    );
  }

  itemTapped(event, selectedQuiz) {
    console.log('selectedQuiz', selectedQuiz);
    this.navCtrl.push(QuizSolvedResponsesPage, { item: selectedQuiz });
  }

  findItems(ev: any) {
    this.rest.quizesTryedOfPerson(this.userID).subscribe(
      (data) => {
        let val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.quizes = data.filter((item) => {
            return (item.quizName.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
      },
      err => { console.log(err); alert("Can't connect to server"); }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizesSolvedPage');
  }

}
