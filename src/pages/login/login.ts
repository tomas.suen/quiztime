import { Component } from '@angular/core';
import { MenuController, IonicPage, NavController, ToastController } from 'ionic-angular';
import { ListPage } from '../list/list'
import { RegisterPage } from '../register/register';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  account: { username: string, password: string } = {
    username: 'joze',
    password: 'meBoza'
  };


  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public menu: MenuController,
    public rest: RestProvider
  ) {
    sessionStorage.clear();
    localStorage.clear();
    // this.menu.enable(false, "sideMenu")
  };


  // Attempt to login in through our User service
  doLogin() {

    console.log(JSON.stringify(this.account));

    //S1 kako naredim da user ne more it nazaj
    this.rest.login(this.account).subscribe(x => {
      if (x === null) {
        alert("Wrong username or password");
      }
      else {
        localStorage.setItem("user", JSON.stringify(x));

        this.navCtrl.setRoot(ListPage);
        this.menu.enable(true, "sideMenu")
      }
    }, err => {
      console.log("napaka v prijavi", err);
    });



  }

  register() {
    this.menu.enable(true, "sideMenu")
    this.navCtrl.push(RegisterPage);
  }
}
