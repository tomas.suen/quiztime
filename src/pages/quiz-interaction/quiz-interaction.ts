import { Component } from '@angular/core';
import { AlertController, ToastController } from 'ionic-angular';
import { IonicPage, NavController, NavParams, ActionSheetController, Platform } from 'ionic-angular';
import { TimerProvider } from '../../providers/timer/timer';
import { StatisticsPage } from '../statistics/statistics';
import { QuizSolverProvider } from '../../providers/quiz-solver/quiz-solver';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the QuizInteractionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-quiz-interaction',
  templateUrl: 'quiz-interaction.html',
})
export class QuizInteractionPage {
  vprasanje: any;
  quiz: any;
  poskus: any;

  odgovor: any;
  trenutni: any;

  timerLabel: string;
  timerTime: number;
  intervalTimer: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public actionsheetCtrl: ActionSheetController,
    public alert: AlertController,
    public platform: Platform,
    public timer: TimerProvider,
    public quizSolver: QuizSolverProvider,
    public rest: RestProvider,
    public toastCtrl: ToastController) {

    this.trenutni = navParams.get("trenutni");
    this.quiz = JSON.parse(localStorage.getItem("kviz"));
    this.poskus = JSON.parse(localStorage.getItem("poskus"));
    this.vprasanje = JSON.parse(localStorage.getItem("vprasanja"))[this.trenutni];

    console.log("prvo quiz", this.quiz.get);
    console.log("prvo vprasanje", this.vprasanje.answers);
    this.vprasanje.answers = JSON.parse(this.vprasanje.answers);

    console.log("vprasanje: ", this.vprasanje);

    let zac = quizSolver.solvedChecker(this.vprasanje.id);

    zac.forEach(e => {
      this.vprasanje.answers[
        this.vprasanje.answers.findIndex(x => x.anwserID === e.selectedAnswer)
      ].checked = true;
    });
    console.log("ze reseno", zac);
  }

  ionViewDidLoad() {

    if (this.trenutni == 0) {
      //console.log("ZAČEL SEM KVIZ")
      this.timer.setTimer(this.quiz.duration * 60);
      this.timerTime = this.timer.getTimer();

      sessionStorage.setItem("hintUsed", "false");
      sessionStorage.setItem("statUsed", "false");
      sessionStorage.setItem("bargainUsed", "false");

    } else {
      //console.log("DELAM NA KVIZU")
      this.timerTime = this.timer.getTimer();
    }

    //console.log("KONAČL SEM ION LOAD")
  }

  ionViewDidLeave() {
    //console.log("ZAPUSTU SM SCREEN")
    clearInterval(this.intervalTimer);
  }

  ionViewDidEnter() {
    //console.log("ION ENTER")
    this.timerTime = this.timer.getTimer();
    this.intervalTimer = setInterval(() => {
      //console.log("ZMANJŠUJEM TIMER")
      this.timerTime = this.timer.tick();
      this.timerLabel = this.timer.toString();
      if (this.timerTime <= 0) {
        alert("KONEC");
        clearInterval(this.intervalTimer);

        this.odgovori();

        //this.navCtrl.push(QuizInteractionPage, { "trenutni": this.trenutni + 1 }, { animation: 'ios-transition', animate: true, direction: 'right' })
      }
      //console.log("this timer: ", this.timerTime);
    }, 1000);
  }

  next() {
    this.odgovori();
    this.navCtrl.push(QuizInteractionPage, { "trenutni": this.trenutni + 1 }, { animation: 'ios-transition', animate: true, direction: 'right' })
  }

  nazaj() {
    this.odgovori();
    this.navCtrl.pop({ animation: 'ios-transition', animate: true, direction: 'back' });
  }

  finish() {
    this.odgovori();
    this.quizSolver.finish(this.quiz, this.navCtrl);
    console.log("Finish clicked");
  }

  showHelp() {
    let btn = [];
    if (this.vprasanje.helpallowed.includes("hint") && sessionStorage.getItem("hintUsed") !== "true") {
      btn.push({
        text: 'Hint',
        icon: !this.platform.is('ios') ? 'text' : null,
        handler: () => {
          sessionStorage.setItem("hintUsed", "true");
          const toast = this.toastCtrl.create({
            message: this.vprasanje.hint,
            duration: 5000,
            position: 'middle',
            showCloseButton: true,
            closeButtonText: 'Ok'
          });
          toast.present();
          console.log('hint clicked');
        }
      });
    }

    if (this.vprasanje.helpallowed.includes("stat") && sessionStorage.getItem("statUsed") !== "true") {
      btn.push({
        text: 'Statistic',
        icon: !this.platform.is('ios') ? 'stats' : null,
        handler: () => {

          sessionStorage.setItem("statUsed", "true");
          this.rest.statisticsOfQestion(this.vprasanje.id).subscribe(stat => {
            this.navCtrl.push(StatisticsPage, {
              "kviz": this.quiz, "vprasanje": this.vprasanje, "stat": stat
            });
          }
          )

        }
      });
    }

    if (this.vprasanje.helpallowed.includes("bargain") && sessionStorage.getItem("bargainUsed") !== "true") {
      btn.push({
        text: 'Bargain',
        icon: !this.platform.is('ios') ? 'star-half' : null,
        handler: () => {
          sessionStorage.setItem("bargainUsed", "true");

          let izbris = this.vprasanje.anwserMax - this.vprasanje.anwserMin;

          for (var i = 0; i < izbris;) {
            let zac = Math.floor(Math.random() * this.vprasanje.answers.length);

            if (this.vprasanje.answers[zac].valid === false) {
              this.vprasanje.answers.splice(zac, 1);
              i++;
            }
          }


        }
      });
    }

    btn.push({
      text: 'Cancel',
      role: 'cancel', // will always sort to be on the bottom
      icon: !this.platform.is('ios') ? 'close' : null,
      handler: () => {
        console.log('Cancel clicked');
      }
    });

    let actionSheet2 = this.actionsheetCtrl.create({
      title: 'Help available',
      cssClass: 'action-sheets-basic-page',
      buttons: btn
    });
    actionSheet2.present();
  }

  odgovori() {

    let seznam = [];
    if (this.vprasanje.multipleAnwser === 1) {
      seznam = this.vprasanje.answers.filter(x => x.checked == true).map(function (a) { return a.anwserID });

      //console.log("odgovori", seznam.map(function (a) { return a.anwserID }));
    }
    else if (this.odgovor != undefined) {
      console.log("odgovorilSi", this.odgovor);
      seznam.push(this.odgovor);
    }
    this.quizSolver.addAnwser(seznam, this.poskus.id, this.vprasanje.id);
  }

  preveriCheckbox(item) {
    this.odgovori();
  }
}
