import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizInteractionPage } from './quiz-interaction';

@NgModule({
  declarations: [
    QuizInteractionPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizInteractionPage),
  ],
  exports: [
    QuizInteractionPage
  ]
})
export class QuizInteractionPageModule {}
