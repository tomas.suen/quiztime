import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';

/**
 * Generated class for the QuizSolvedResponsesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var Chart;
@IonicPage()
@Component({
  selector: 'page-quiz-solved-responses',
  templateUrl: 'quiz-solved-responses.html',
})
export class QuizSolvedResponsesPage {

  responses =[];
  quiz:any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public rest: RestProvider) {

    if (this.rest.loginCheck(navCtrl)) {
      let userID= JSON.parse(localStorage.getItem("user")).id
      this.quiz = navParams.get("item");
      console.log('userID', userID);
      console.log('quiz', this.quiz);

      this.rest.responsesOfPerson(userID).subscribe(
        (data) => {
          console.log(data);
        
          let val = this.quiz.id;

          // if the value is an empty string don't filter the items
          
            this.responses = data.filter((item) => {
              return (item.quiz_id === val);
            })

            this.responses.map(a=>a.responseStart = moment(a.responseStart).format('DD.MMM.YYYY HH:mm'))
          
        },
        err => { console.log(err); alert("Connections problems"); }
      );
    }
    console.log(navParams);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizSolvedResponsesPage');


    /*var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels: ["Fail", "Ok"],
        datasets: [{
          data: [60, 40],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 235, 162, 0.2)',
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 235, 162, 1)',
          ],
          borderWidth: 1
        }]
      }
    });*/
  }

}
