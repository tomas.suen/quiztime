import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuizSolvedResponsesPage } from './quiz-solved-responses';

@NgModule({
  declarations: [
    QuizSolvedResponsesPage,
  ],
  imports: [
    IonicPageModule.forChild(QuizSolvedResponsesPage),
  ],
  exports: [
    QuizSolvedResponsesPage
  ]
})
export class QuizSolvedResponsesPageModule {}
