import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class RestProvider {

  url: string = 'http://192.168.43.76:3000/'; //mobitel
  //url: string = 'http://192.168.1.137:3000/'; //doma

  constructor(public http: Http) {
    console.log('Hello RestProvider Provider');
  }

  getUserById(id: number) {
    return this.http.get(this.url + 'user/?id=' + id).map(res => {
      return res.json();
    });
  }

  getQuizes() {
    return this.http.get(this.url + 'quiz').map(res => {
      return res.json();
    })
  }

  getQuizDetailsByid(id: number) {
    return this.http.get(this.url + 'quiz/complete/' + id).map(res => {
      return res.json();
    })
  }

  getTime() {
    return this.http.get(this.url + 'timestamp').map(res => {
      return res.json();
    })
  }

  getTimePromise() {
    return this.http.get(this.url + 'timestamp').toPromise().then(response => response.json())
      ;
  }

  announceBeginigOfQuiz(userId: number, quizId: number, timestamp: any) {
    return this.http.post(this.url + 'response', {
      "user_id": userId,
      "quiz_id": quizId,
      "responseStart": timestamp
    }).map(res => {
      console.log("response", res);
      return res.json();
    });
  }

  submitAnwser(newAnwser: any) {
    return this.http.post(this.url + 'anwser', newAnwser).map(res => {
      return res.json();
    });
  }

  submitAnwsers(anwsers: any) {
    return this.http.post(this.url + 'answer/multiple', anwsers).map(res => {
      return res.json();
    });
  }

  updateResponse(response: any) {

    return this.http.put(this.url + 'response/' + response.id, response).map(res => {
      return res.json();
    });
  }

  statisticsOfQestion(QuestionID: number) {
    return this.http.get(this.url + 'answer/question/' + QuestionID).map(res => {
      return res.json();
    })
  }

  responsesOfPerson(PersonID: number) {
    return this.http.get(this.url + 'response/user/' + PersonID).map(res => {
      return res.json();
    })
  }

  quizesTryedOfPerson(PersonID: number) {
    return this.http.get(this.url + 'quiz/responded/' + PersonID).map(res => {
      return res.json();
    })
  }

  login(person: any) {
    return this.http.post(this.url + 'user/authenticate', person).map(res => {
      return res.json();
    });
  }

  register(person: any) {
    let account: { username: string, password: string } = {
      username: person.username,
      password: person.password1
    };
    return this.http.post(this.url + 'user', account).map(res => {
      return res.json();
    });
  }

  usernameCheck(person){
    return this.http.get(this.url + 'user/name/' + person.username).map(res => {
      return res.json();
    })
  }

  loginCheck(navCtrl: NavController){
    let u = JSON.parse(localStorage.getItem("user"))
    if( u === null){
      alert("You are not loged in")
      navCtrl.setRoot(LoginPage);
      return false;
    }
    return true;
  }

  userUpdatePassword(person){
    let account: { id: number, username: string, password: string } = {
      id: person.id,
      username: person.username,
      password: person.password1
    };
    return this.http.put(this.url + 'user/'+ account.id, account).map(res => {
      return res.json();
    });
  }
}
