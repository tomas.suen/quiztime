import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { RestProvider } from '../rest/rest';
import * as moment from 'moment';

import { NavController, NavParams, Nav } from 'ionic-angular';
import { QuizResultPage } from '../../pages/quiz-result/quiz-result';
import { ListPage } from '../../pages/list/list';

/*
  Generated class for the QuizSolverProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class QuizSolverProvider {

  odgovor = [];

  constructor(public http: Http, public rest: RestProvider) {
    console.log('Hello QuizSolverProvider Provider');
  }

  addAnwser(selectedAnswers, responseID: number, questionID: number) {
    if (this.odgovor.findIndex(x => x.questions_id == questionID) > -1) {
      this.odgovor = this.odgovor.filter(function (o) {
        return o.questions_id !== questionID;
      })
    }

    selectedAnswers.forEach(i => {
      this.odgovor.push({
        'response_id': responseID,
        'questions_id': questionID,
        "selectedAnswer": Number(i)
      })
    });

    console.log("trenutni odgovori", JSON.stringify(this.odgovor));

  }

  timeCheck(startTime, endTime, allowedSeconds: number) {
    let start = moment(startTime);
    let end = moment(endTime);
    let timediffms = end.diff(start) - 10000;  // difference in ms
    console.log("timeDiff", timediffms);
    return timediffms < (allowedSeconds * 1000);
  }

  finish(quiz: any, navCtr: NavController) {
    console.log("finished quiz", quiz);
    console.log("koncniOdgovori", this.odgovor);
    console.log("poskus", localStorage.getItem("poskus"));

    let poskus = JSON.parse(localStorage.getItem("poskus"));
    let startTime = JSON.parse(localStorage.getItem("poskus")).responseStart;

    this.rest.getTime().subscribe((time) => {
      if (!this.timeCheck(startTime, time.timestamp, quiz.duration * 60)) {
        alert("to late");
        navCtr.setRoot(ListPage)
      }
      poskus.responseEnd = time.timestamp;

      this.rest.submitAnwsers(this.odgovor).subscribe((vneseno) => {
        poskus.score = this.izracun(quiz, this.odgovor)
        this.rest.updateResponse(poskus).subscribe((noviPoskus) => {
          navCtr.setRoot(QuizResultPage, { 'data': { 'poskus': poskus, 'quiz': quiz, 'odgovori': this.odgovor } });
        });
      });
    });
  }

  reset() {
    this.odgovor = [];
  }

  solvedChecker(qestionID: number) {
    return this.odgovor.filter(x => x.questions_id === qestionID);
  }

  izracun(quiz, odgovor) {
    var maxscore = 0
    var score = 0;
    var odg = [];


    quiz.question.forEach(q => {
      let zac = [];
      JSON.parse(q.answers).forEach(a => {
        if (a.valid === true) {
          maxscore++;
          zac.push(a.anwserID)
        }
      })
      odg.push({ "qestionID": q.id, "validAnwsers": zac })
    })

    var mapica = odg.map(function (d) { return d['qestionID']; })
    odgovor.forEach(x => {
      let zacasniOdgovor = odg[mapica.indexOf(x.questions_id)];
      if (zacasniOdgovor.validAnwsers.indexOf(x.selectedAnswer) > -1) {
        score += 1 / zacasniOdgovor.validAnwsers.length;
      }
      else if (zacasniOdgovor.validAnwsers.length > 1)
      {
        score -= 1 / zacasniOdgovor.validAnwsers.length;
      }


    })

    return Math.floor((score / maxscore) * 100)
  }

}
