import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the TimerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TimerProvider {

  timer: number;
  constructor(public http: Http) {
    console.log('Hello TimerProvider Provider');
  }

  setTimer(timer: number){
    this.timer = timer;
  }

  getTimer(): number{
    return this.timer;
  }

  toString(): string{
    return Math.floor(this.timer/60).toString() + ":" + (this.timer%60).toString();
  }

  tick():number{
    this.timer -= 1;
    return this.timer;
  }



}
